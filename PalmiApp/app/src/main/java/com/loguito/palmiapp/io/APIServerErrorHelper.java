package com.loguito.palmiapp.io;

import android.content.Context;

import com.loguito.palmiapp.R;
import com.loguito.palmiapp.models.Error;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by beto on 2/18/17.
 */

public class APIServerErrorHelper {
    private static final int DEFAULT_ERROR_CODE = 500;

    public static Error getDefaultError(Context context) {
        return new Error(DEFAULT_ERROR_CODE, context.getString(R.string.default_end_user_message_error), context.getString(R.string.default_developer_message_error));
    }

    public static Error getMappedServerError(Context context, Retrofit retrofit, Response response) {
        Error serverError;
        // Try to parse the server error into our model, if cannot, use a default error
        try {
            Converter<ResponseBody, Error> errorConverter = retrofit.responseBodyConverter(Error.class, new Annotation[0]);
            serverError = errorConverter.convert(response.errorBody());

        } catch (IOException e) {
            serverError = getDefaultError(context);
        }
        return serverError;
    }
}
