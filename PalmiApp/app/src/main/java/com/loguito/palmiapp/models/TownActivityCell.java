package com.loguito.palmiapp.models;

/**
 * Created by beto on 2/19/17.
 */

public interface TownActivityCell {
    String getText();
    String getImageUrl();
    int getIconId();
}
