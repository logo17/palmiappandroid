package com.loguito.palmiapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.loguito.palmiapp.R;
import com.loguito.palmiapp.models.BusRoute;
import com.loguito.palmiapp.models.RouteSchedule;

import java.util.List;

/**
 * Created by beto on 2/21/17.
 */

public class BusScheduleDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int SECTION_TYPE = 0;
    private final int CELL_TYPE = 1;

    private List<BusRoute> busRoutes;

    public void setBusRoutes(List<BusRoute> busRoutes) {
        this.busRoutes = busRoutes;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if(viewType == SECTION_TYPE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_section, parent, false);
            return new BusScheduleSectionViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_bus_schedule_detail, parent, false);
            return new BusScheduleDetailViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof BusScheduleSectionViewHolder) {
            BusScheduleSectionViewHolder busScheduleSectionViewHolder = (BusScheduleSectionViewHolder) holder;
            busScheduleSectionViewHolder.bindHolder(this.busRoutes.get(getSectionIndex(position)).getRouteDirection());
        } else if(holder instanceof BusScheduleDetailViewHolder) {
            BusScheduleDetailViewHolder busScheduleDetailViewHolder = (BusScheduleDetailViewHolder) holder;
            RouteSchedule routeSchedule = this.busRoutes.get(getSectionIndex(position)).getRouteSchedules().get(getBusRouteIndexList(position));
            busScheduleDetailViewHolder.bindHolder(routeSchedule.getScheduleDays(), routeSchedule.getScheduleOptions());
        }
    }

    @Override
    public int getItemCount() {
        int result = 0;
        if (busRoutes != null && busRoutes.size() > 0) {
            for(BusRoute busRoute : this.busRoutes) {
                result = result + busRoute.getRouteSchedules().size() + 1; // add each section and its list size
            }
        }
        return result;
    }

    @Override
    public int getItemViewType(int position) {
        if(isSection(position)) {
            return SECTION_TYPE;
        } else {
            return CELL_TYPE;
        }
    }

    private boolean isSection(int position) {
        boolean result = false;
        int sectionIndex = 0;
        for (BusRoute busRoute : this.busRoutes) {
            if(sectionIndex == position) {
                result = true;
                break;
            }
            sectionIndex += (busRoute.getRouteSchedules().size() + 1);
        }
        return result;
    }

    private int getSectionIndex(int position) {
        int result = -1;
        int currentIndex = 0;
        for (int i=0; i<this.busRoutes.size(); ++i) {
            currentIndex += busRoutes.get(i).getRouteSchedules().size() + 1;
            if (position < currentIndex) {
                result = i;
                break;
            }
        }
        return result;
    }

    private int getBusRouteIndexList(int position) {
        int result;
        int sectionIndex = getSectionIndex(position);
        int overlapIndex = 1;
        for(int i = 0; i < sectionIndex; ++i) {
            overlapIndex += (busRoutes.get(i).getRouteSchedules().size() + 1);
        }
        result = position - overlapIndex;
        return result;
    }

    private class BusScheduleSectionViewHolder extends RecyclerView.ViewHolder {
        private TextView sectionTextView;
        public BusScheduleSectionViewHolder(View itemView) {
            super(itemView);

            sectionTextView = (TextView) itemView.findViewById(R.id.title);
        }

        public void bindHolder(String sectionText) {
            sectionTextView.setText(sectionText);
        }
    }

    private class BusScheduleDetailViewHolder extends RecyclerView.ViewHolder {

        private TextView scheduleDayTextView;
        private TextView scheduleOptionsTextView;

        public BusScheduleDetailViewHolder(View itemView) {
            super(itemView);

            scheduleDayTextView = (TextView) itemView.findViewById(R.id.schedule_day);
            scheduleOptionsTextView = (TextView) itemView.findViewById(R.id.schedule_options);
        }

        public void bindHolder (String scheduleDay, List<String> scheduleOptions) {
            scheduleDayTextView.setText(scheduleDay);
            StringBuilder sb = new StringBuilder();
            for (String option : scheduleOptions) {
                sb.append(option).append("\n");
            }
            scheduleOptionsTextView.setText(sb.toString());
        }
    }

}
