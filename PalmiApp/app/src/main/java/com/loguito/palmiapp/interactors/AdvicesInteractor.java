package com.loguito.palmiapp.interactors;

import android.content.Context;

import com.loguito.palmiapp.io.APIServerErrorHelper;
import com.loguito.palmiapp.io.PalmiAppApiService;
import com.loguito.palmiapp.io.callback.GetAdviceListListener;
import com.loguito.palmiapp.models.AdviceSection;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by beto on 2/19/17.
 */

public class AdvicesInteractor {
    private PalmiAppApiService appApiService;
    private Retrofit retrofit;
    private Context context;

    public AdvicesInteractor(PalmiAppApiService appApiService, Retrofit retrofit, Context context) {
        this.appApiService = appApiService;
        this.retrofit = retrofit;
        this.context = context;
    }

    public void getAdviceList(final GetAdviceListListener listener) {
        Call<List<AdviceSection>> call = appApiService.getTownAdvices();

        if (call != null) {
            call.enqueue(new Callback<List<AdviceSection>>() {
                @Override
                public void onResponse(Call<List<AdviceSection>> call, Response<List<AdviceSection>> response) {
                    if (response.isSuccessful()) {
                        listener.onGetAdviceListSuccess(response.body());
                    } else {
                        listener.onServerError(APIServerErrorHelper.getMappedServerError(context, retrofit, response));
                    }
                }

                @Override
                public void onFailure(Call<List<AdviceSection>> call, Throwable t) {
                    listener.onServerError(APIServerErrorHelper.getDefaultError(context));
                }
            });
        }
    }
}
