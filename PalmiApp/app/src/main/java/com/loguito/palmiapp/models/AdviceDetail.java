package com.loguito.palmiapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by beto on 2/19/17.
 */

public class AdviceDetail implements Parcelable {
    @SerializedName("description")
    private String description;
    @SerializedName("date")
    private Date date;
    @SerializedName("image_url")
    private String imageUrl;

    public AdviceDetail(String description, Date date, String imageUrl) {
        this.description = description;
        this.date = date;
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public Date getDate() {
        return date;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.description);
        dest.writeLong(this.date != null ? this.date.getTime() : -1);
        dest.writeString(this.imageUrl);
    }

    protected AdviceDetail(Parcel in) {
        this.description = in.readString();
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        this.imageUrl = in.readString();
    }

    public static final Parcelable.Creator<AdviceDetail> CREATOR = new Parcelable.Creator<AdviceDetail>() {
        @Override
        public AdviceDetail createFromParcel(Parcel source) {
            return new AdviceDetail(source);
        }

        @Override
        public AdviceDetail[] newArray(int size) {
            return new AdviceDetail[size];
        }
    };
}
