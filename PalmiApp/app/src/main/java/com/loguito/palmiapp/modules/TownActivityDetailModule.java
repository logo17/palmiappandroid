package com.loguito.palmiapp.modules;

import com.loguito.palmiapp.adapters.DetailActivityAdapter;
import com.loguito.palmiapp.presenters.TownActivityDetailPresenter;
import com.loguito.palmiapp.presenters.TownActivityPresenter;
import com.loguito.palmiapp.ui.views.TownActivityDetailView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by beto on 2/19/17.
 */
@Module
public class TownActivityDetailModule {
    private TownActivityDetailView view;

    public TownActivityDetailModule(TownActivityDetailView view) {
        this.view = view;
    }

    @Provides
    public TownActivityDetailView providesTownActivityDetailView() {
        return view;
    }

    @Provides
    public TownActivityDetailPresenter providesTownActivityDetailPresenter(TownActivityDetailView view) {
        return new TownActivityDetailPresenter(view);
    }

    @Provides
    public DetailActivityAdapter providesDetailActivityAdapter() {
        return new DetailActivityAdapter();
    }
}
