package com.loguito.palmiapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by beto on 2/18/17.
 */

public class Error {
    @SerializedName("code")
    private int code;
    @SerializedName("enduser_message")
    private String endUserMessage;
    @SerializedName("developer_message")
    private String developerMessage;

    public Error(int code, String endUserMessage, String developerMessage) {
        this.code = code;
        this.endUserMessage = endUserMessage;
        this.developerMessage = developerMessage;
    }

    public int getCode() {
        return code;
    }

    public String getEndUserMessage() {
        return endUserMessage;
    }

    public String getDeveloperMessage() {
        return developerMessage;
    }
}
