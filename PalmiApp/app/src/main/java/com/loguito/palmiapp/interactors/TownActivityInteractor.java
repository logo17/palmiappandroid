package com.loguito.palmiapp.interactors;

import android.content.Context;

import com.loguito.palmiapp.io.APIServerErrorHelper;
import com.loguito.palmiapp.io.PalmiAppApiService;
import com.loguito.palmiapp.io.callback.GetTownActivitiesListener;
import com.loguito.palmiapp.models.TownActivity;
import com.loguito.palmiapp.models.TownActivityType;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by beto on 2/18/17.
 */

public class TownActivityInteractor {
    private PalmiAppApiService appApiService;
    private Retrofit retrofit;
    private Context context;

    public final static String SPORT_TYPE = "sports";
    public final static String CULTURAL_TYPE = "cultural";

    public TownActivityInteractor(PalmiAppApiService appApiService, Retrofit retrofit, Context context) {
        this.appApiService = appApiService;
        this.retrofit = retrofit;
        this.context = context;
    }

    public void getTownActivities(final GetTownActivitiesListener listener, TownActivityType type) {
        String path = "";
        switch (type) {
            case SPORT:
                path = SPORT_TYPE;
                break;

            case CULTURAL:
                path = CULTURAL_TYPE;
                break;

            case PARADE:
                break;

            default:
                path = SPORT_TYPE;
                break;

        }

        Call<List<TownActivity>> call = appApiService.getTownActivities(path);
        if(call != null) {
            call.enqueue(new Callback<List<TownActivity>>() {
                @Override
                public void onResponse(Call<List<TownActivity>> call, Response<List<TownActivity>> response) {
                    if (response.isSuccessful()) {
                        listener.onGetTownActivitiesSuccess(response.body());
                    } else {
                        listener.onServerError(APIServerErrorHelper.getMappedServerError(context, retrofit, response));
                    }
                }

                @Override
                public void onFailure(Call<List<TownActivity>> call, Throwable t) {
                    listener.onServerError(APIServerErrorHelper.getDefaultError(context));
                }
            });
        }
    }

}
