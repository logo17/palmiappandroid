package com.loguito.palmiapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.loguito.palmiapp.R;
import com.loguito.palmiapp.common.DateFormatter;
import com.loguito.palmiapp.models.AdviceDetail;
import com.loguito.palmiapp.models.AdviceSection;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by beto on 2/19/17.
 */

public class AdvicesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int SECTION_TYPE = 0;
    private final int CELL_TYPE = 1;

    private List<AdviceSection> list;

    public void setList(List<AdviceSection> list) {
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if(viewType == SECTION_TYPE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_section, parent, false);
            return new SectionViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_advice_detail, parent, false);
            return new AdviceDetailViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof SectionViewHolder) {
            SectionViewHolder sectionViewHolder = (SectionViewHolder) holder;
            sectionViewHolder.bindHolder(this.list.get(getSectionIndex(position)).getName());
        } else if(holder instanceof AdviceDetailViewHolder) {
            AdviceDetailViewHolder adviceDetailViewHolder = (AdviceDetailViewHolder) holder;
            AdviceDetail adviceDetail = this.list.get(getSectionIndex(position)).getDetails().get(getAdviceIndexList(position));
            adviceDetailViewHolder.bindHolder(adviceDetail.getImageUrl(), adviceDetail.getDescription(), DateFormatter.formatDate(adviceDetail.getDate()));
        }
    }

    @Override
    public int getItemCount() {
        int result = 0;
        if (list != null && list.size() > 0) {
            for(AdviceSection adviceSection : this.list) {
                result = result + adviceSection.getDetails().size() + 1; // add each section and its list size
            }
        }
        return result;
    }

    @Override
    public int getItemViewType(int position) {
        if(isSection(position)) {
            return SECTION_TYPE;
        } else {
            return CELL_TYPE;
        }
    }

    private boolean isSection(int position) {
        boolean result = false;
        int sectionIndex = 0;
        for (AdviceSection adviceSection : this.list) {
            if(sectionIndex == position) {
                result = true;
                break;
            }
            sectionIndex += (adviceSection.getDetails().size() + 1);
        }
        return result;
    }

    private int getSectionIndex(int position) {
        int result = -1;
        int currentIndex = 0;
        for (int i=0; i<this.list.size(); ++i) {
            currentIndex += list.get(i).getDetails().size() + 1;
            if (position < currentIndex) {
                result = i;
                break;
            }
        }
        return result;
    }

    private int getAdviceIndexList(int position) {
        int result;
        int sectionIndex = getSectionIndex(position);
        int overlapIndex = 1;
        for(int i = 0; i < sectionIndex; ++i) {
            overlapIndex += (list.get(i).getDetails().size() + 1);
        }
        result = position - overlapIndex;
        return result;
    }

    private class SectionViewHolder extends RecyclerView.ViewHolder {

        private TextView title;

        public SectionViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
        }

        private void bindHolder(String titleText) {
            this.title.setText(titleText);
        }
    }

    private class AdviceDetailViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView circleImageView;
        private TextView description;
        private TextView date;

        public AdviceDetailViewHolder(View itemView) {
            super(itemView);

            circleImageView = (CircleImageView) itemView.findViewById(R.id.circle_image);
            description = (TextView) itemView.findViewById(R.id.description);
            date = (TextView) itemView.findViewById(R.id.date);
        }

        private void bindHolder(String imageUrl, String descriptionText, String dateText) {
            this.description.setText(descriptionText);
            this.date.setText(dateText);
            if (!TextUtils.isEmpty(imageUrl)) {
                Glide.with(itemView.getContext()).load(imageUrl).into(circleImageView);
            }
        }
    }
}
