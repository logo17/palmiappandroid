package com.loguito.palmiapp;

import android.content.Context;

import com.loguito.palmiapp.interactors.AdvicesInteractor;
import com.loguito.palmiapp.interactors.BusScheduleInteractor;
import com.loguito.palmiapp.interactors.TownActivityInteractor;
import com.loguito.palmiapp.modules.InteractorModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by beto on 2/11/17.
 */

@Singleton
@Component(
        modules = {
                PalmiAppModule.class,
                InteractorModule.class
        }
)
public interface PalmiAppComponent {
    Context getContext();

    TownActivityInteractor getTownActivityInteractor();

    AdvicesInteractor getAdvidesInteractor();

    BusScheduleInteractor getBusScheduleInteractor();

}