package com.loguito.palmiapp.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by beto on 2/19/17.
 */

public class DateFormatter {
    public static String formatDate(Date date) {
        DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
        return df.format(date);
    }
}
