package com.loguito.palmiapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by beto on 2/21/17.
 */

public class BusSchedule implements Parcelable {
    @SerializedName("corporation_name")
    private String corporationName;
    @SerializedName("town_name")
    private String townName;
    @SerializedName("routes")
    private List<BusRoute> busRoutes;

    public BusSchedule(String corporationName, String townName, List<BusRoute> busRoutes) {
        this.corporationName = corporationName;
        this.townName = townName;
        this.busRoutes = busRoutes;
    }

    public String getCorporationName() {
        return corporationName;
    }

    public String getTownName() {
        return townName;
    }

    public List<BusRoute> getBusRoutes() {
        return busRoutes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.corporationName);
        dest.writeString(this.townName);
        dest.writeTypedList(this.busRoutes);
    }

    protected BusSchedule(Parcel in) {
        this.corporationName = in.readString();
        this.townName = in.readString();
        this.busRoutes = in.createTypedArrayList(BusRoute.CREATOR);
    }

    public static final Parcelable.Creator<BusSchedule> CREATOR = new Parcelable.Creator<BusSchedule>() {
        @Override
        public BusSchedule createFromParcel(Parcel source) {
            return new BusSchedule(source);
        }

        @Override
        public BusSchedule[] newArray(int size) {
            return new BusSchedule[size];
        }
    };
}
