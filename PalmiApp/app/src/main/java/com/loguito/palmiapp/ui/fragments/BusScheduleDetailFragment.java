package com.loguito.palmiapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.loguito.palmiapp.PalmiAppComponent;
import com.loguito.palmiapp.R;
import com.loguito.palmiapp.adapters.BusScheduleDetailAdapter;
import com.loguito.palmiapp.common.BaseFragment;
import com.loguito.palmiapp.common.BasePresenter;
import com.loguito.palmiapp.models.BusRoute;
import com.loguito.palmiapp.ui.activities.BusScheduleDetailActivity;

import java.util.List;

import butterknife.BindView;

/**
 * Created by beto on 2/22/17.
 */

public class BusScheduleDetailFragment extends BaseFragment {
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    private BusScheduleDetailAdapter adapter;
    private List<BusRoute> routes;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        routes = getArguments().getParcelableArrayList(BusScheduleDetailActivity.MODEL_PARAM_NAME);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new BusScheduleDetailAdapter();
        initRecyclerView();
    }

    private void initRecyclerView() {
        if(recyclerView != null) {
            adapter.setBusRoutes(routes);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(CONTEXT));
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_bus_schedule_detail;
    }

    @Override
    protected void setUpComponent(PalmiAppComponent component) {

    }
}
