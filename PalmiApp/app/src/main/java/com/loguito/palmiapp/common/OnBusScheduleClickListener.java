package com.loguito.palmiapp.common;

import com.loguito.palmiapp.models.BusSchedule;

/**
 * Created by beto on 2/22/17.
 */

public interface OnBusScheduleClickListener {
    void onBusScheduleClicked(BusSchedule busSchedule);
}
