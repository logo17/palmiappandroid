package com.loguito.palmiapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.loguito.palmiapp.R;
import com.loguito.palmiapp.models.DescriptionTownActivityCell;
import com.loguito.palmiapp.models.DetailNameTownActivityCell;
import com.loguito.palmiapp.models.LocationTownActivityCell;
import com.loguito.palmiapp.models.SectionTownActivityCell;
import com.loguito.palmiapp.models.TownActivityCell;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by beto on 2/19/17.
 */

public class DetailActivityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int SECTION_TYPE = 100;
    private final int DESCRIPTION_TYPE = 101;
    private final int DETAIL_NAME_TYPE = 102;
    private final int LOCATION_TYPE = 103;


    private List<TownActivityCell> list;

    public void setList(List<TownActivityCell> list) {
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case SECTION_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_section, parent, false);
                return new SectionTownActivityViewHolder(view);

            case DESCRIPTION_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_town_activity_description, parent, false);
                return new DescriptionTownActivityViewHolder(view);

            case DETAIL_NAME_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_town_activity_detail_name, parent, false);
                return new DetailNameTownActivityViewHolder(view);

            case LOCATION_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_town_activity_location, parent, false);
                return new LocationTownActivityViewHolder(view);

            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TownActivityCell model = list.get(position);
        if (holder instanceof SectionTownActivityViewHolder) {
            SectionTownActivityViewHolder sectionTownActivityViewHolder = (SectionTownActivityViewHolder) holder;
            sectionTownActivityViewHolder.bindHolder(model.getText());
        } else if (holder instanceof DescriptionTownActivityViewHolder) {
            DescriptionTownActivityViewHolder descriptionTownActivityViewHolder = (DescriptionTownActivityViewHolder) holder;
            descriptionTownActivityViewHolder.bindHolder(model.getText(), model.getImageUrl(), model.getIconId());
        } else if (holder instanceof DetailNameTownActivityViewHolder) {
            DetailNameTownActivityViewHolder detailNameTownActivityViewHolder = (DetailNameTownActivityViewHolder) holder;
            detailNameTownActivityViewHolder.bindHolder(model.getText());
        } else if (holder instanceof LocationTownActivityViewHolder) {
            LocationTownActivityViewHolder locationTownActivityViewHolder = (LocationTownActivityViewHolder) holder;
            locationTownActivityViewHolder.bindHolder(model.getText(), model.getImageUrl());
        }
    }

    @Override
    public int getItemCount() {
        int result = 0;
        if (list != null && list.size() > 0) {
            result = list.size();
        }
        return result;
    }

    @Override
    public int getItemViewType(int position) {
        int result = -1;
        TownActivityCell model = list.get(position);
        if (model instanceof SectionTownActivityCell) {
            result = SECTION_TYPE;
        } else if (model instanceof DescriptionTownActivityCell) {
            result = DESCRIPTION_TYPE;
        } else if (model instanceof DetailNameTownActivityCell) {
            result = DETAIL_NAME_TYPE;
        } else if (model instanceof LocationTownActivityCell) {
            result = LOCATION_TYPE;
        }
        return result;
    }

    private class SectionTownActivityViewHolder extends RecyclerView.ViewHolder {

        private TextView title;

        public SectionTownActivityViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
        }

        private void bindHolder(String titleText) {
            this.title.setText(titleText);
        }

    }

    private class DetailNameTownActivityViewHolder extends RecyclerView.ViewHolder {

        private TextView title;

        public DetailNameTownActivityViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
        }

        private void bindHolder(String titleText) {
            this.title.setText(titleText);
        }
    }

    private class LocationTownActivityViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private ImageView mapImage;

        public LocationTownActivityViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            mapImage = (ImageView) itemView.findViewById(R.id.map_image);
        }

        private void bindHolder(String titleText, String mapImageUrl) {
            this.title.setText(titleText);
            if (!TextUtils.isEmpty(mapImageUrl)) {
                Glide.with(itemView.getContext()).load(mapImageUrl).into(mapImage);
            }
        }
    }

    private class DescriptionTownActivityViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private CircleImageView icon;

        public DescriptionTownActivityViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            icon = (CircleImageView) itemView.findViewById(R.id.circle_image);
        }

        private void bindHolder(String titleText, String iconImageUrl, int iconImageId) {
            this.title.setText(titleText);
            if (!TextUtils.isEmpty(iconImageUrl)) {
                Glide.with(itemView.getContext()).load(iconImageUrl).placeholder(iconImageId).into(icon);
            } else if (iconImageId != -1) {
                icon.setBackground(itemView.getContext().getDrawable(iconImageId));
            }
        }
    }

}
