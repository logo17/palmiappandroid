package com.loguito.palmiapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.loguito.palmiapp.R;
import com.loguito.palmiapp.adapters.TabAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by beto on 2/18/17.
 */

public class ActivitiesPagerFragment extends Fragment {

    @BindView(R.id.pager)
    ViewPager viewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_town_activities, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setupViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        TabAdapter tabAdapter = new TabAdapter(getActivity().getSupportFragmentManager());
        tabAdapter.addFragment(new SportActivitiesFragment(),getString(R.string.sports_tab_title));
        tabAdapter.addFragment(new CulturalActivitiesFragment(),getString(R.string.cultural_tab_title));
        viewPager.setAdapter(tabAdapter);
    }

}
