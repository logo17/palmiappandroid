package com.loguito.palmiapp;

import android.app.Application;
import android.content.Context;

import com.loguito.palmiapp.interactors.TownActivityInteractor;
import com.loguito.palmiapp.io.PalmiAppApiAdapter;
import com.loguito.palmiapp.io.PalmiAppApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by beto on 2/11/17.
 */

@Module
public class PalmiAppModule {

    private PalmiApp app;

    public PalmiAppModule(PalmiApp app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public Application provideApp(){
        return app;
    }

    @Provides
    @Singleton
    public Context provideContext(){
        return app;
    }

    @Provides
    @Singleton public Retrofit provideRetrofitInstance() {
        return PalmiAppApiAdapter.getInstance();
    }

    @Provides
    @Singleton
    public PalmiAppApiService providePalmiAppApiService(Retrofit retrofit){
        return retrofit.create(PalmiAppApiService.class);
    }


}