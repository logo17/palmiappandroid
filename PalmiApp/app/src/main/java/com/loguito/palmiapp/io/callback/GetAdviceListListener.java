package com.loguito.palmiapp.io.callback;

import com.loguito.palmiapp.models.AdviceSection;

import java.util.List;

/**
 * Created by beto on 2/19/17.
 */

public interface GetAdviceListListener extends ServerError {
    void onGetAdviceListSuccess(List<AdviceSection> advices);
}
