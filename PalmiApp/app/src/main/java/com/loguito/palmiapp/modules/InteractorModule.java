package com.loguito.palmiapp.modules;

import android.content.Context;

import com.loguito.palmiapp.interactors.AdvicesInteractor;
import com.loguito.palmiapp.interactors.BusScheduleInteractor;
import com.loguito.palmiapp.interactors.TownActivityInteractor;
import com.loguito.palmiapp.io.PalmiAppApiService;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by beto on 2/11/17.
 */

@Module
public class InteractorModule {
    @Provides
    public TownActivityInteractor provideTownActivityInteractor(PalmiAppApiService appApiService, Retrofit retrofit, Context context) {
        return new TownActivityInteractor(appApiService, retrofit, context);
    }

    @Provides
    public AdvicesInteractor provideAdvicesInteractor(PalmiAppApiService appApiService, Retrofit retrofit, Context context) {
        return new AdvicesInteractor(appApiService, retrofit, context);
    }

    @Provides
    public BusScheduleInteractor provideBusScheduleInteractor(PalmiAppApiService appApiService, Retrofit retrofit, Context context) {
        return new BusScheduleInteractor(appApiService, retrofit, context);
    }
}
