package com.loguito.palmiapp.models;

/**
 * Created by beto on 2/19/17.
 */

public class SectionTownActivityCell implements TownActivityCell {

    private String sectionName;

    public SectionTownActivityCell(String sectionName) {
        this.sectionName = sectionName;
    }

    @Override
    public String getText() {
        return sectionName;
    }

    @Override
    public String getImageUrl() {
        return null;
    }

    @Override
    public int getIconId() {
        return -1;
    }
}
