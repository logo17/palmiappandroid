package com.loguito.palmiapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.loguito.palmiapp.R;
import com.loguito.palmiapp.common.OnBusScheduleClickListener;
import com.loguito.palmiapp.models.BusSchedule;

import java.util.List;

/**
 * Created by beto on 2/21/17.
 */

public class BusScheduleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<BusSchedule> schedules;
    private OnBusScheduleClickListener listener;

    public void setSchedules(List<BusSchedule> schedules) {
        this.schedules = schedules;
    }

    public void setListener(OnBusScheduleClickListener listener) {
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_bus_schedule_title, parent, false);
        return new BusScheduleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BusSchedule model = schedules.get(position);
        if (holder instanceof BusScheduleViewHolder) {
            BusScheduleViewHolder busScheduleViewHolder = (BusScheduleViewHolder) holder;
            busScheduleViewHolder.bindHolder(model.getCorporationName(), model.getTownName());
        }
    }

    @Override
    public int getItemCount() {
        int result = 0;
        if (schedules != null && schedules.size() > 0) {
            result = schedules.size();
        }
        return result;
    }

    private class BusScheduleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView corporateNameTextView;
        private TextView townNameTextView;
        public BusScheduleViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            corporateNameTextView = (TextView) itemView.findViewById(R.id.corporate_name);
            townNameTextView = (TextView) itemView.findViewById(R.id.town_name);
        }

        public void bindHolder (String corporateName, String townName) {
            corporateNameTextView.setText(corporateName);
            townNameTextView.setText(townName);
        }

        @Override
        public void onClick(View view) {
            listener.onBusScheduleClicked(schedules.get(getAdapterPosition()));
        }
    }

}
