package com.loguito.palmiapp.presenters;

import com.loguito.palmiapp.R;
import com.loguito.palmiapp.interactors.AdvicesInteractor;
import com.loguito.palmiapp.io.callback.GetAdviceListListener;
import com.loguito.palmiapp.models.AdviceSection;
import com.loguito.palmiapp.models.Error;
import com.loguito.palmiapp.ui.views.AdvicesView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by beto on 2/19/17.
 */
public class AdvicesPresenterTest {
    @Mock
    AdvicesInteractor interactor;
    @Mock
    AdvicesView view;
    @Mock
    private List<AdviceSection> advices;
    @Mock
    private Error error;

    private AdvicesPresenter presenter;

    @Captor
    private ArgumentCaptor<GetAdviceListListener> listenerArgumentCaptor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        presenter = new AdvicesPresenter(view, interactor);
    }

    @Test
    public void testGetAdviceList() throws Exception {
        //when
        presenter.getAdviceList();

        //then
        verify(view).showLoadingMessage(eq(R.string.getting_advices_message));
        verify(interactor).getAdviceList(listenerArgumentCaptor.capture());
    }

    @Test
    public void testGetAdvicesSuccess() throws Exception {
        presenter.getAdviceList();

        verify(view).showLoadingMessage(R.string.getting_advices_message);
        verify(interactor).getAdviceList(listenerArgumentCaptor.capture());

        listenerArgumentCaptor.getValue().onGetAdviceListSuccess(advices);

        verify(view).hideLoadingMessage();
        verify(view).onGetAdvicesSuccess(eq(advices));
    }

    @Test
    public void testGetAdvicesFailure() throws Exception {
        presenter.getAdviceList();

        verify(view).showLoadingMessage(R.string.getting_advices_message);
        verify(interactor).getAdviceList(listenerArgumentCaptor.capture());
        Mockito.when(error.getEndUserMessage()).thenReturn("Error al cargar los avisos importantes.");

        listenerArgumentCaptor.getValue().onServerError(error);

        verify(view).hideLoadingMessage();
        verify(view).onGetAdvicesFailure(eq(error.getEndUserMessage()));
    }

}