package com.loguito.palmiapp.common;

import com.loguito.palmiapp.adapters.ActivitiesAdapter;
import com.loguito.palmiapp.models.TownActivity;

/**
 * Created by beto on 2/19/17.
 */

public interface OnTownActivityClickListener {
    void onTownActivityClicked(TownActivity townActivity, ActivitiesAdapter.TownActivityViewHolder holder);
}
