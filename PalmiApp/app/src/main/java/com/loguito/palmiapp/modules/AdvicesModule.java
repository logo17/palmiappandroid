package com.loguito.palmiapp.modules;

import com.loguito.palmiapp.adapters.AdvicesAdapter;
import com.loguito.palmiapp.interactors.AdvicesInteractor;
import com.loguito.palmiapp.presenters.AdvicesPresenter;
import com.loguito.palmiapp.ui.views.AdvicesView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by beto on 2/19/17.
 */
@Module
public class AdvicesModule {
    private AdvicesView view;

    public AdvicesModule(AdvicesView view) {
        this.view = view;
    }

    @Provides
    public AdvicesView providesView() {
        return this.view;
    }

    @Provides
    public AdvicesPresenter providesPresenter(AdvicesView view, AdvicesInteractor interactor){
        return new AdvicesPresenter(view, interactor);
    }

    @Provides
    public AdvicesAdapter providesAdapter() {
        return new AdvicesAdapter();
    }

}
