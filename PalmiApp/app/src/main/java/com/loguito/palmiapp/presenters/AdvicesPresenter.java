package com.loguito.palmiapp.presenters;

import com.loguito.palmiapp.R;
import com.loguito.palmiapp.common.BasePresenter;
import com.loguito.palmiapp.interactors.AdvicesInteractor;
import com.loguito.palmiapp.io.callback.GetAdviceListListener;
import com.loguito.palmiapp.models.AdviceSection;
import com.loguito.palmiapp.models.Error;
import com.loguito.palmiapp.ui.views.AdvicesView;

import java.util.List;

/**
 * Created by beto on 2/19/17.
 */

public class AdvicesPresenter extends BasePresenter implements GetAdviceListListener {
    private AdvicesView view;
    private AdvicesInteractor interactor;

    public AdvicesPresenter(AdvicesView view, AdvicesInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    public void getAdviceList() {
        view.showLoadingMessage(R.string.getting_advices_message);
        interactor.getAdviceList(this);
    }

    @Override
    protected void onStart() {

    }

    @Override
    protected void onStop() {

    }

    @Override
    public void onGetAdviceListSuccess(List<AdviceSection> advices) {
        view.hideLoadingMessage();
        view.onGetAdvicesSuccess(advices);
    }

    @Override
    public void onServerError(Error error) {
        view.hideLoadingMessage();
        view.onGetAdvicesFailure(error.getEndUserMessage());
    }
}
