package com.loguito.palmiapp.modules;

import com.loguito.palmiapp.adapters.BusScheduleAdapter;
import com.loguito.palmiapp.interactors.BusScheduleInteractor;
import com.loguito.palmiapp.presenters.BusSchedulePresenter;
import com.loguito.palmiapp.ui.views.BusScheduleView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by beto on 2/22/17.
 */
@Module
public class BusScheduleModule {

    private BusScheduleView view;

    public BusScheduleModule(BusScheduleView view) {
        this.view = view;
    }

    @Provides
    public BusScheduleView providesBusScheduleView() {
        return this.view;
    }

    @Provides
    public BusSchedulePresenter providesBusSchedulePresenter(BusScheduleView view, BusScheduleInteractor interactor) {
        return new BusSchedulePresenter(view, interactor);
    }

    @Provides
    public BusScheduleAdapter providesBusScheduleAdapter() {
        return new BusScheduleAdapter();
    }

}

