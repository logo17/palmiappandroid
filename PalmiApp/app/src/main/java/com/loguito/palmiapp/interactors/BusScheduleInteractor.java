package com.loguito.palmiapp.interactors;

import android.content.Context;

import com.loguito.palmiapp.io.APIServerErrorHelper;
import com.loguito.palmiapp.io.PalmiAppApiService;
import com.loguito.palmiapp.io.callback.GetBusScheduleListener;
import com.loguito.palmiapp.models.BusSchedule;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by beto on 2/22/17.
 */

public class BusScheduleInteractor {
    private PalmiAppApiService appApiService;
    private Retrofit retrofit;
    private Context context;

    public BusScheduleInteractor(PalmiAppApiService appApiService, Retrofit retrofit, Context context) {
        this.appApiService = appApiService;
        this.retrofit = retrofit;
        this.context = context;
    }

    public void getBusSchedule(final GetBusScheduleListener listener) {
        Call<List<BusSchedule>> call = appApiService.getBusSchedule();

        if (call != null) {
            call.enqueue(new Callback<List<BusSchedule>>() {
                @Override
                public void onResponse(Call<List<BusSchedule>> call, Response<List<BusSchedule>> response) {
                    if (response.isSuccessful()) {
                        listener.onGetBusScheduleSuccess(response.body());
                    } else {
                        listener.onServerError(APIServerErrorHelper.getMappedServerError(context, retrofit, response));
                    }
                }

                @Override
                public void onFailure(Call<List<BusSchedule>> call, Throwable t) {
                    listener.onServerError(APIServerErrorHelper.getDefaultError(context));
                }
            });
        }
    }

}
