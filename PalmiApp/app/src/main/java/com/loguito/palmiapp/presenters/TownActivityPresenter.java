package com.loguito.palmiapp.presenters;

import com.loguito.palmiapp.R;
import com.loguito.palmiapp.common.BasePresenter;
import com.loguito.palmiapp.interactors.TownActivityInteractor;
import com.loguito.palmiapp.io.callback.GetTownActivitiesListener;
import com.loguito.palmiapp.models.Error;
import com.loguito.palmiapp.models.TownActivityType;
import com.loguito.palmiapp.models.TownActivity;
import com.loguito.palmiapp.ui.views.TownActivitiesView;

import java.util.List;

/**
 * Created by beto on 2/18/17.
 */

public class TownActivityPresenter extends BasePresenter implements GetTownActivitiesListener {

    private TownActivityInteractor interactor;
    private TownActivitiesView view;

    public TownActivityPresenter(TownActivityInteractor interactor, TownActivitiesView view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    protected void onStart() {

    }

    @Override
    protected void onStop() {

    }

    public void getTownActivities(TownActivityType type) {
        switch (type) {
            case SPORT:
                view.showLoadingMessage(R.string.getting_sport_activities_message);
                interactor.getTownActivities(this, TownActivityType.SPORT);
                break;

            case CULTURAL:
                view.showLoadingMessage(R.string.getting_cultural_activities_message);
                interactor.getTownActivities(this, TownActivityType.CULTURAL);
                break;

            case PARADE:
                break;

            default:
                break;
        }
    }

    @Override
    public void onGetTownActivitiesSuccess(List<TownActivity> activities) {
        view.hideLoadingMessage();
        view.onGetActivitiesSuccess(activities);
    }

    @Override
    public void onServerError(Error error) {
        view.hideLoadingMessage();
        view.onGetActivitiesFailure(error.getEndUserMessage());
    }
}
