package com.loguito.palmiapp.ui.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.loguito.palmiapp.R;
import com.loguito.palmiapp.common.BaseActivity;
import com.loguito.palmiapp.models.TownActivity;
import com.loguito.palmiapp.ui.fragments.TownActivityDetailFragment;

/**
 * Created by beto on 2/19/17.
 */

public class TownActivityDetailActivity extends BaseActivity {

    public static final String MODEL_PARAM_NAME = "town_activity";
    private TownActivity townActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        townActivity = getIntent().getParcelableExtra(MODEL_PARAM_NAME);
        super.onCreate(savedInstanceState);
        setUpToolbar(townActivity.getName(), true);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_single_fragment;
    }

    @Override
    protected Fragment createFragment() {
        TownActivityDetailFragment fragment = new TownActivityDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(MODEL_PARAM_NAME, townActivity);
        fragment.setArguments(bundle);
        return fragment;
    }

}
