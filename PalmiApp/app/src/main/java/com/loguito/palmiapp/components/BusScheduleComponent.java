package com.loguito.palmiapp.components;

import com.loguito.palmiapp.PalmiAppComponent;
import com.loguito.palmiapp.adapters.BusScheduleAdapter;
import com.loguito.palmiapp.common.ActivityScope;
import com.loguito.palmiapp.modules.BusScheduleModule;
import com.loguito.palmiapp.presenters.BusSchedulePresenter;
import com.loguito.palmiapp.ui.fragments.BusScheduleFragment;

import dagger.Component;

/**
 * Created by beto on 2/22/17.
 */
@ActivityScope
@Component(
        dependencies = PalmiAppComponent.class,
        modules = BusScheduleModule.class
)
public interface BusScheduleComponent {
    void inject(BusScheduleFragment fragment);

    BusSchedulePresenter getPresenter();

    BusScheduleAdapter getAdapter();
}
