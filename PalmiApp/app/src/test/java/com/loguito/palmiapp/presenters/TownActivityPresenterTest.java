package com.loguito.palmiapp.presenters;

import com.loguito.palmiapp.R;
import com.loguito.palmiapp.interactors.TownActivityInteractor;
import com.loguito.palmiapp.io.callback.GetTownActivitiesListener;
import com.loguito.palmiapp.models.Error;
import com.loguito.palmiapp.models.TownActivity;
import com.loguito.palmiapp.models.TownActivityType;
import com.loguito.palmiapp.ui.views.TownActivitiesView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by beto on 2/18/17.
 */
public class TownActivityPresenterTest {

    @Mock
    private TownActivityInteractor interactor;
    @Mock
    private TownActivitiesView view;
    @Mock
    private List<TownActivity> activities;
    @Mock
    private Error error;
    @Captor
    private ArgumentCaptor<GetTownActivitiesListener> listenerArgumentCaptor;

    private TownActivityPresenter presenter;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        presenter = new TownActivityPresenter(interactor, view);
    }

    @Test
    public void testGetTownSportActivities() throws Exception {
        presenter.getTownActivities(TownActivityType.SPORT);

        verify(view).showLoadingMessage(R.string.getting_sport_activities_message);
        verify(interactor).getTownActivities(listenerArgumentCaptor.capture(), eq(TownActivityType.SPORT));
    }

    @Test
    public void testGetTownSportActivitiesSuccess() throws Exception {
        presenter.getTownActivities(TownActivityType.SPORT);

        verify(view).showLoadingMessage(R.string.getting_sport_activities_message);
        verify(interactor).getTownActivities(listenerArgumentCaptor.capture(), eq(TownActivityType.SPORT));

        listenerArgumentCaptor.getValue().onGetTownActivitiesSuccess(activities);

        verify(view).hideLoadingMessage();
        verify(view).onGetActivitiesSuccess(eq(activities));
    }

    @Test
    public void testGetTownSportActivitiesFailure() throws Exception {
        presenter.getTownActivities(TownActivityType.SPORT);

        verify(view).showLoadingMessage(R.string.getting_sport_activities_message);
        verify(interactor).getTownActivities(listenerArgumentCaptor.capture(), eq(TownActivityType.SPORT));
        Mockito.when(error.getEndUserMessage()).thenReturn("Error al cargar las actividades deportivas");

        listenerArgumentCaptor.getValue().onServerError(error);

        verify(view).hideLoadingMessage();
        verify(view).onGetActivitiesFailure(eq(error.getEndUserMessage()));
    }

    @Test
    public void testGetTownCulturalActivities() throws Exception {
        presenter.getTownActivities(TownActivityType.CULTURAL);

        verify(view).showLoadingMessage(R.string.getting_cultural_activities_message);
        verify(interactor).getTownActivities(listenerArgumentCaptor.capture(), eq(TownActivityType.CULTURAL));
    }

    @Test
    public void testGetTownCulturalActivitiesSuccess() throws Exception {
        presenter.getTownActivities(TownActivityType.CULTURAL);

        verify(view).showLoadingMessage(R.string.getting_cultural_activities_message);
        verify(interactor).getTownActivities(listenerArgumentCaptor.capture(), eq(TownActivityType.CULTURAL));

        listenerArgumentCaptor.getValue().onGetTownActivitiesSuccess(activities);

        verify(view).hideLoadingMessage();
        verify(view).onGetActivitiesSuccess(eq(activities));
    }

    @Test
    public void testGetTownCulturalActivitiesFailure() throws Exception {
        presenter.getTownActivities(TownActivityType.CULTURAL);

        verify(view).showLoadingMessage(R.string.getting_cultural_activities_message);
        verify(interactor).getTownActivities(listenerArgumentCaptor.capture(), eq(TownActivityType.CULTURAL));
        Mockito.when(error.getEndUserMessage()).thenReturn("Error al cargar las actividades culturales");

        listenerArgumentCaptor.getValue().onServerError(error);

        verify(view).hideLoadingMessage();
        verify(view).onGetActivitiesFailure(eq(error.getEndUserMessage()));
    }

}