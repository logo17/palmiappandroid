package com.loguito.palmiapp.models;

/**
 * Created by beto on 2/18/17.
 */

public enum TownActivityType {
    SPORT, CULTURAL, PARADE;
}
