package com.loguito.palmiapp.ui.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.loguito.palmiapp.R;
import com.loguito.palmiapp.common.BaseActivity;
import com.loguito.palmiapp.models.BusRoute;
import com.loguito.palmiapp.models.BusSchedule;
import com.loguito.palmiapp.ui.fragments.BusScheduleDetailFragment;

import java.util.ArrayList;

/**
 * Created by beto on 2/22/17.
 */

public class BusScheduleDetailActivity extends BaseActivity {

    public static final String MODEL_PARAM_NAME = "bus_schedule";
    private BusSchedule busSchedule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        busSchedule = getIntent().getParcelableExtra(MODEL_PARAM_NAME);
        super.onCreate(savedInstanceState);
        setUpToolbar(busSchedule.getTownName(), true); //TODO: Add bus schedule title here
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_single_fragment;
    }

    @Override
    protected Fragment createFragment() {
        BusScheduleDetailFragment fragment = new BusScheduleDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(MODEL_PARAM_NAME, new ArrayList<>(busSchedule.getBusRoutes()));
        fragment.setArguments(bundle);
        return fragment;
    }
}
