package com.loguito.palmiapp.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.loguito.palmiapp.PalmiApp;
import com.loguito.palmiapp.PalmiAppComponent;
import com.loguito.palmiapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by beto on 2/6/16.
 */
public abstract class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    /**
     * onStart will be set a view with getLayout method
     * Also inject views and dependencies
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        injectViews();

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = createFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
    }

    /**
     * This method should be called in the child activity if it has a toolbar.
     * @param title
     */
    protected void setUpToolbar(String title, boolean hasActionButton) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        getSupportActionBar().setTitle(title);
        if(hasActionButton){
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * It will determine with layout is gonna be inflated in the activity
     * @return
     */
    protected abstract int getLayout();

    /**
     * It will create a fragment which is going to be inserted in the activity
     * @return
     */
    protected abstract Fragment createFragment();

    /**
     * This method is going to prepare butterknife for inflating all views
     */
    protected void injectViews(){
        ButterKnife.bind(this);
    }






}
