package com.loguito.palmiapp.modules;

import com.loguito.palmiapp.adapters.ActivitiesAdapter;
import com.loguito.palmiapp.interactors.TownActivityInteractor;
import com.loguito.palmiapp.presenters.TownActivityPresenter;
import com.loguito.palmiapp.ui.views.TownActivitiesView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by beto on 2/18/17.
 */
@Module
public class TownActivityModule {
    private TownActivitiesView view;

    public TownActivityModule(TownActivitiesView view) {
        this.view = view;
    }

    @Provides
    public TownActivitiesView providesView() {
        return view;
    }

    @Provides
    public TownActivityPresenter providesPresenter(TownActivityInteractor interactor, TownActivitiesView view) {
        return new TownActivityPresenter(interactor, view);
    }

    @Provides
    public ActivitiesAdapter providesActivitiesAdapter() {
        return new ActivitiesAdapter();
    }
}
