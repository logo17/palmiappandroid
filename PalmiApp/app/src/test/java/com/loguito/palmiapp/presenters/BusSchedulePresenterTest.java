package com.loguito.palmiapp.presenters;

import com.loguito.palmiapp.R;
import com.loguito.palmiapp.interactors.BusScheduleInteractor;
import com.loguito.palmiapp.io.callback.GetBusScheduleListener;
import com.loguito.palmiapp.models.BusSchedule;
import com.loguito.palmiapp.models.Error;
import com.loguito.palmiapp.ui.views.BusScheduleView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by beto on 2/22/17.
 */
public class BusSchedulePresenterTest {
    @Mock
    BusScheduleView view;
    @Mock
    BusScheduleInteractor interactor;
    @Mock
    List<BusSchedule> busSchedules;
    @Mock
    Error error;

    @Captor
    private ArgumentCaptor<GetBusScheduleListener> argumentCaptor;

    private BusSchedulePresenter presenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        presenter = new BusSchedulePresenter(view, interactor);
    }

    @Test
    public void testGetBusScheduleCalled() throws Exception {
        //when
        presenter.getBusSchedule();

        //then
        verify(view).showLoadingMessage(eq(R.string.getting_bus_schedule_message));
        verify(interactor).getBusSchedule(argumentCaptor.capture());
    }

    @Test
    public void testGetBusScheduleSuccess() throws Exception {
        //when
        presenter.getBusSchedule();

        //then
        verify(view).showLoadingMessage(eq(R.string.getting_bus_schedule_message));
        verify(interactor).getBusSchedule(argumentCaptor.capture());

        argumentCaptor.getValue().onGetBusScheduleSuccess(busSchedules);

        verify(view).hideLoadingMessage();
        verify(view).onGetBusScheduleSuccess(eq(busSchedules));
    }

    @Test
    public void testGetBusScheduleFailure() throws Exception {
        //when
        presenter.getBusSchedule();

        //then
        verify(view).showLoadingMessage(eq(R.string.getting_bus_schedule_message));
        verify(interactor).getBusSchedule(argumentCaptor.capture());

        argumentCaptor.getValue().onServerError(error);

        verify(view).hideLoadingMessage();
        verify(view).onGetBusScheduleFailure(eq(error.getEndUserMessage()));
    }

}