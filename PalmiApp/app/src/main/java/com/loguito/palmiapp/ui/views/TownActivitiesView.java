package com.loguito.palmiapp.ui.views;

import com.loguito.palmiapp.models.TownActivity;

import java.util.List;

/**
 * Created by beto on 2/18/17.
 */

public interface TownActivitiesView {
    void onGetActivitiesSuccess(List<TownActivity> activities);
    void onGetActivitiesFailure(String message);
    void showLoadingMessage(int messageId);
    void hideLoadingMessage();

}
