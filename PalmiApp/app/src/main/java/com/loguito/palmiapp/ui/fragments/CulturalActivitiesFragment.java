package com.loguito.palmiapp.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.loguito.palmiapp.PalmiAppComponent;
import com.loguito.palmiapp.R;
import com.loguito.palmiapp.adapters.ActivitiesAdapter;
import com.loguito.palmiapp.common.BaseFragment;
import com.loguito.palmiapp.common.BasePresenter;
import com.loguito.palmiapp.common.OnTownActivityClickListener;
import com.loguito.palmiapp.components.DaggerTownCulturalActivitiesComponent;
import com.loguito.palmiapp.models.TownActivity;
import com.loguito.palmiapp.models.TownActivityType;
import com.loguito.palmiapp.modules.TownActivityModule;
import com.loguito.palmiapp.presenters.TownActivityPresenter;
import com.loguito.palmiapp.ui.activities.TownActivityDetailActivity;
import com.loguito.palmiapp.ui.views.TownActivitiesView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by beto on 2/19/17.
 */

public class CulturalActivitiesFragment extends BaseFragment implements TownActivitiesView, OnTownActivityClickListener {

    @BindView(R.id.recyclerview)
    RecyclerView activitiesRecyclerView;
    @BindView(R.id.fab)
    FloatingActionButton createActivityButton;

    @Inject
    TownActivityPresenter presenter;

    @Inject
    ActivitiesAdapter adapter;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
        presenter.getTownActivities(TownActivityType.CULTURAL);
    }

    private void initRecyclerView() {
        if(activitiesRecyclerView != null) {
            adapter.setClickListener(this);
            activitiesRecyclerView.setHasFixedSize(true);
            activitiesRecyclerView.setLayoutManager(new LinearLayoutManager(CONTEXT));
            activitiesRecyclerView.setAdapter(adapter);
        }
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_generic_activities;
    }

    @Override
    protected void setUpComponent(PalmiAppComponent component) {
        DaggerTownCulturalActivitiesComponent.builder()
                .palmiAppComponent(component)
                .townActivityModule(new TownActivityModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void onGetActivitiesSuccess(List<TownActivity> activities) {
        adapter.setList(activities);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onGetActivitiesFailure(String message) {

    }

    @Override
    public void showLoadingMessage(int messageId) {
        super.showLoadingProgressBar(getString(messageId));
    }

    @Override
    public void hideLoadingMessage() {
        super.hideLoadingProgressBar();
    }

    @Override
    public void onTownActivityClicked(TownActivity townActivity, ActivitiesAdapter.TownActivityViewHolder holder) {
        Intent intent = new Intent(getActivity(), TownActivityDetailActivity.class);
        intent.putExtra(TownActivityDetailActivity.MODEL_PARAM_NAME, townActivity);

        ActivityOptionsCompat activityOptions =
                ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                        new Pair<View, String>(holder.cardImage, getString(R.string.detail_image_transition_name)));
        ActivityCompat.startActivity(getActivity(), intent, activityOptions.toBundle());
    }
}
