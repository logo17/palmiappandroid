package com.loguito.palmiapp.io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by beto on 2/11/17.
 */

public class PalmiAppApiAdapter {
    private static Retrofit RETROFIT;

    public static Retrofit getInstance(){
        Gson gson = new GsonBuilder()
                .setDateFormat("dd/mm/yyyy")
                .create();

        //The adapter will be a singleton
        if(RETROFIT == null){
            RETROFIT = new Retrofit.Builder()
                    .baseUrl(PalmiAppApiConstants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return RETROFIT;
    }
}
