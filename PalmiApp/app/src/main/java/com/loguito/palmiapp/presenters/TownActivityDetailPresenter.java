package com.loguito.palmiapp.presenters;

import com.loguito.palmiapp.R;
import com.loguito.palmiapp.common.BasePresenter;
import com.loguito.palmiapp.common.DateFormatter;
import com.loguito.palmiapp.models.DescriptionTownActivityCell;
import com.loguito.palmiapp.models.DetailNameTownActivityCell;
import com.loguito.palmiapp.models.LocationTownActivityCell;
import com.loguito.palmiapp.models.SectionTownActivityCell;
import com.loguito.palmiapp.models.TownActivity;
import com.loguito.palmiapp.models.TownActivityCell;
import com.loguito.palmiapp.ui.views.TownActivityDetailView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by beto on 2/19/17.
 */

public class TownActivityDetailPresenter extends BasePresenter {

    private TownActivityDetailView view;

    public TownActivityDetailPresenter(TownActivityDetailView view) {
        this.view = view;
    }

    @Override
    protected void onStart() {

    }

    @Override
    protected void onStop() {

    }

    public void prepareTownActivityDetailInformation(TownActivity townActivity) {
        List<TownActivityCell> list = new ArrayList<>();
        list.add(new SectionTownActivityCell("Descripción"));
        list.add(new DescriptionTownActivityCell(townActivity.getDescription(), null, R.drawable.ic_assignment_black)); // no image
        list.add(new SectionTownActivityCell("Fecha"));
        if (townActivity.getDate() != null) {
            list.add(new DescriptionTownActivityCell(DateFormatter.formatDate(townActivity.getDate()), null, R.drawable.ic_date)); //  no image
        }
        if (townActivity.getLocation() != null) {
            list.add(new SectionTownActivityCell("Ubicación"));
            list.add(new LocationTownActivityCell(
                    townActivity.getLocation().getLocationName(),
                    townActivity.getLocation().getLatitude(),
                    townActivity.getLocation().getLongitude(),
                    townActivity.getLocation().getLocationImageUrl()));
        }
        if (townActivity.getOrganizer() != null) {
            list.add(new SectionTownActivityCell("Organizador"));
            list.add(new DescriptionTownActivityCell(townActivity.getOrganizer().getName(), townActivity.getOrganizer().getImageUrl(), R.drawable.ic_account_circle)); // no icon id
        }

        list.add(new SectionTownActivityCell("Costo"));
        list.add(new DescriptionTownActivityCell(townActivity.getAmmount(), null, R.drawable.ic_payment));
        view.loadTownActivityDetailInformation(list);
    }

}
