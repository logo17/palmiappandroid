package com.loguito.palmiapp.components;

import com.loguito.palmiapp.PalmiAppComponent;
import com.loguito.palmiapp.adapters.DetailActivityAdapter;
import com.loguito.palmiapp.common.ActivityScope;
import com.loguito.palmiapp.modules.TownActivityDetailModule;
import com.loguito.palmiapp.presenters.TownActivityDetailPresenter;
import com.loguito.palmiapp.ui.fragments.TownActivityDetailFragment;

import dagger.Component;

/**
 * Created by beto on 2/19/17.
 */
@ActivityScope
@Component(
        dependencies = PalmiAppComponent.class,
        modules = TownActivityDetailModule.class
)
public interface TownActivityDetailComponent {
    void inject(TownActivityDetailFragment fragment);

    TownActivityDetailPresenter getPresenter();

    DetailActivityAdapter getAdapter();
}
