package com.loguito.palmiapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by beto on 2/21/17.
 */

public class BusRoute implements Parcelable {
    @SerializedName("route_direction")
    private String routeDirection;
    @SerializedName("route_schedule")
    private List<RouteSchedule> routeSchedules;

    public BusRoute(String routeDirection, List<RouteSchedule> routeSchedules) {
        this.routeDirection = routeDirection;
        this.routeSchedules = routeSchedules;
    }

    public String getRouteDirection() {
        return routeDirection;
    }

    public List<RouteSchedule> getRouteSchedules() {
        return routeSchedules;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.routeDirection);
        dest.writeTypedList(this.routeSchedules);
    }

    protected BusRoute(Parcel in) {
        this.routeDirection = in.readString();
        this.routeSchedules = in.createTypedArrayList(RouteSchedule.CREATOR);
    }

    public static final Parcelable.Creator<BusRoute> CREATOR = new Parcelable.Creator<BusRoute>() {
        @Override
        public BusRoute createFromParcel(Parcel source) {
            return new BusRoute(source);
        }

        @Override
        public BusRoute[] newArray(int size) {
            return new BusRoute[size];
        }
    };
}
