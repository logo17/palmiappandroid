package com.loguito.palmiapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.loguito.palmiapp.PalmiAppComponent;
import com.loguito.palmiapp.R;
import com.loguito.palmiapp.adapters.DetailActivityAdapter;
import com.loguito.palmiapp.common.BaseFragment;
import com.loguito.palmiapp.common.BasePresenter;
import com.loguito.palmiapp.components.DaggerTownActivityDetailComponent;
import com.loguito.palmiapp.models.TownActivity;
import com.loguito.palmiapp.models.TownActivityCell;
import com.loguito.palmiapp.modules.TownActivityDetailModule;
import com.loguito.palmiapp.presenters.TownActivityDetailPresenter;
import com.loguito.palmiapp.ui.activities.TownActivityDetailActivity;
import com.loguito.palmiapp.ui.views.TownActivityDetailView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by beto on 2/19/17.
 */

public class TownActivityDetailFragment extends BaseFragment implements TownActivityDetailView {

    private TownActivity townActivity;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.town_activity_background_image)
    ImageView backgroundImageView;

    @Inject
    TownActivityDetailPresenter presenter;

    @Inject
    DetailActivityAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        townActivity = getArguments().getParcelable(TownActivityDetailActivity.MODEL_PARAM_NAME);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
        Glide.with(getContext()).load(townActivity.getBackgroundUrlImage()).into(backgroundImageView);
        presenter.prepareTownActivityDetailInformation(townActivity);
    }

    private void initRecyclerView() {
        if(recyclerView != null) {
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(CONTEXT));
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_town_activity_detail;
    }

    @Override
    protected void setUpComponent(PalmiAppComponent component) {
        DaggerTownActivityDetailComponent.builder()
                .palmiAppComponent(component)
                .townActivityDetailModule(new TownActivityDetailModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void loadTownActivityDetailInformation(List<TownActivityCell> list) {
        adapter.setList(list);
        adapter.notifyDataSetChanged();
    }
}
