package com.loguito.palmiapp.common;

import com.loguito.palmiapp.io.PalmiAppApiService;
import com.loguito.palmiapp.models.AdviceSection;
import com.loguito.palmiapp.models.BusSchedule;
import com.loguito.palmiapp.models.TownActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Path;
import retrofit2.mock.BehaviorDelegate;

/**
 * Created by beto on 2/18/17.
 */

public class MockSuccessPalmiAppApiService implements PalmiAppApiService {

    private final BehaviorDelegate<PalmiAppApiService> delegate;

    public MockSuccessPalmiAppApiService(BehaviorDelegate<PalmiAppApiService> delegate) {
        this.delegate = delegate;
    }

    @Override
    public Call<List<TownActivity>> getTownActivities(@Path("activity_type") String activityType) {

        List<TownActivity> result = new ArrayList<>();
        TownActivity townActivity = new TownActivity();
        result.add(townActivity);
        result.add(townActivity);

        return delegate.returningResponse(result).getTownActivities(activityType);
    }

    @Override
    public Call<List<AdviceSection>> getTownAdvices() {
        List<AdviceSection> result = new ArrayList<>();
        AdviceSection adviceSection = new AdviceSection();
        result.add(adviceSection);
        result.add(adviceSection);
        result.add(adviceSection);

        return delegate.returningResponse(result).getTownAdvices();
    }

    @Override
    public Call<List<BusSchedule>> getBusSchedule() {
        List<BusSchedule> result = new ArrayList<>();
        BusSchedule busSchedule = new BusSchedule("Comtrasuli S.A.", "Palmichal",null);
        result.add(busSchedule);
        busSchedule = new BusSchedule("Comtrasuli S.A.", "Piedra Blanca",null);
        result.add(busSchedule);
        busSchedule = new BusSchedule("Comtrasuli S.A.", "Ciudad Colón",null);
        result.add(busSchedule);
        busSchedule = new BusSchedule("Comtrasuli S.A.", "Puriscal",null);
        result.add(busSchedule);

        return delegate.returningResponse(result).getBusSchedule();
    }
}
