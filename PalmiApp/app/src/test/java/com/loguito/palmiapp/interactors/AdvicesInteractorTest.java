package com.loguito.palmiapp.interactors;

import android.content.Context;

import com.loguito.palmiapp.common.MockFailurePalmiAppApiService;
import com.loguito.palmiapp.common.MockSuccessPalmiAppApiService;
import com.loguito.palmiapp.io.PalmiAppApiService;
import com.loguito.palmiapp.io.callback.GetAdviceListListener;
import com.loguito.palmiapp.models.AdviceSection;
import com.loguito.palmiapp.models.Error;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.lang.annotation.Annotation;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

import static org.mockito.Mockito.verify;

/**
 * Created by beto on 2/19/17.
 */
public class AdvicesInteractorTest {
    private MockRetrofit mockRetrofit;
    private Retrofit retrofit;
    @Mock
    private PalmiAppApiService palmiAppApiService;
    @Mock
    private Context context;

    private AdvicesInteractor interactor;
    private GetAdviceListListener getAdviceListListener;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        retrofit = new Retrofit.Builder().baseUrl("http://testurl.com")
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NetworkBehavior networkBehavior = NetworkBehavior.create();
        mockRetrofit = new MockRetrofit.Builder(retrofit)
                .networkBehavior(networkBehavior)
                .build();

        getAdviceListListener = new GetAdviceListListener() {
            @Override
            public void onGetAdviceListSuccess(List<AdviceSection> advices) {

            }

            @Override
            public void onServerError(Error error) {

            }
        };

    }

    @Test
    public void testGetAdvicesCalled() throws Exception {
        //given
        interactor = new AdvicesInteractor(palmiAppApiService, retrofit, context);
        AdvicesInteractor interactorSpy = Mockito.spy(interactor);

        //when
        interactorSpy.getAdviceList(getAdviceListListener);

        //then
        verify(palmiAppApiService).getTownAdvices();
    }

    @Test
    public void testGetAdvicesDeserializeSuccess() throws Exception {
        //given
        BehaviorDelegate<PalmiAppApiService> delegate = mockRetrofit.create(PalmiAppApiService.class);
        MockSuccessPalmiAppApiService mockSuccessBaseApiService = new MockSuccessPalmiAppApiService(delegate);

        //when
        Call<List<AdviceSection>> call = mockSuccessBaseApiService.getTownAdvices();
        Response<List<AdviceSection>> response = call.execute();

        //then
        Assert.assertNotNull(response.body());
        Assert.assertNull(response.errorBody());
        Assert.assertEquals(response.body().size(),3); // Because we add to dummy objects to the response in the @MockSuccessPalmiAppService
    }

    @Test
    public void testGetAdvicesFailure() throws Exception {
        //given
        BehaviorDelegate<PalmiAppApiService> delegate = mockRetrofit.create(PalmiAppApiService.class);
        MockFailurePalmiAppApiService mockFailureBaseApiService = new MockFailurePalmiAppApiService(delegate);

        //when
        Call<List<AdviceSection>> call = mockFailureBaseApiService.getTownAdvices();
        Response<List<AdviceSection>> response = call.execute();

        Converter<ResponseBody, Error> errorConverter = retrofit.responseBodyConverter(Error.class, new Annotation[0]);
        Error error = errorConverter.convert(response.errorBody());

        //then
        Assert.assertFalse(response.isSuccessful());
        Assert.assertNull(response.body());
        Assert.assertNotNull(response.errorBody());
        Assert.assertEquals(404, error.getCode());
        Assert.assertEquals("Error al obtener los avisos importantes.", error.getEndUserMessage());
        Assert.assertEquals("Unknown activity type", error.getDeveloperMessage());
    }

}