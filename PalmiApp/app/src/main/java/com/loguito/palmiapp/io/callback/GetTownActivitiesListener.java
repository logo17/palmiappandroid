package com.loguito.palmiapp.io.callback;

import com.loguito.palmiapp.models.TownActivity;

import java.util.List;

/**
 * Created by beto on 2/18/17.
 */

public interface GetTownActivitiesListener extends ServerError {
    void onGetTownActivitiesSuccess(List<TownActivity> activities);
}
