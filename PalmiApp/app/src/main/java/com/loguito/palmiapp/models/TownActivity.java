package com.loguito.palmiapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by beto on 2/18/17.
 */

public class TownActivity implements Parcelable {

    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("location")
    private ActivityLocation location;
    @SerializedName("date")
    private Date date;
    @SerializedName("ammount")
    private String ammount;
    @SerializedName("background_image")
    private String backgroundUrlImage;
    @SerializedName("images")
    private List<String> activityImages;
    @SerializedName("organizer")
    private ActivityOrganizer organizer;

    public TownActivity(String name, String description, ActivityLocation location,
                        Date date, String ammount, String backgroundUrlImage,
                        List<String> activityImages, ActivityOrganizer organizer) {
        this.name = name;
        this.description = description;
        this.location = location;
        this.date = date;
        this.ammount = ammount;
        this.backgroundUrlImage = backgroundUrlImage;
        this.activityImages = activityImages;
        this.organizer = organizer;
    }

    public TownActivity() {
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ActivityLocation getLocation() {
        return location;
    }

    public Date getDate() {
        return date;
    }

    public String getAmmount() {
        return ammount;
    }

    public String getBackgroundUrlImage() {
        return backgroundUrlImage;
    }

    public List<String> getActivityImages() {
        return activityImages;
    }

    public ActivityOrganizer getOrganizer() {
        return organizer;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeParcelable(this.location, flags);
        dest.writeLong(this.date != null ? this.date.getTime() : -1);
        dest.writeString(this.ammount);
        dest.writeString(this.backgroundUrlImage);
        dest.writeStringList(this.activityImages);
        dest.writeParcelable(this.organizer, flags);
    }

    protected TownActivity(Parcel in) {
        this.name = in.readString();
        this.description = in.readString();
        this.location = in.readParcelable(ActivityLocation.class.getClassLoader());
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        this.ammount = in.readString();
        this.backgroundUrlImage = in.readString();
        this.activityImages = in.createStringArrayList();
        this.organizer = in.readParcelable(ActivityOrganizer.class.getClassLoader());
    }

    public static final Parcelable.Creator<TownActivity> CREATOR = new Parcelable.Creator<TownActivity>() {
        @Override
        public TownActivity createFromParcel(Parcel source) {
            return new TownActivity(source);
        }

        @Override
        public TownActivity[] newArray(int size) {
            return new TownActivity[size];
        }
    };
}
