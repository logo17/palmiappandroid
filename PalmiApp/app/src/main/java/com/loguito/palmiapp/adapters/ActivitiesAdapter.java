package com.loguito.palmiapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.loguito.palmiapp.R;
import com.loguito.palmiapp.common.OnTownActivityClickListener;
import com.loguito.palmiapp.models.TownActivity;

import java.util.List;

/**
 * Created by beto on 2/18/17.
 */

public class ActivitiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<TownActivity> list;
    private OnTownActivityClickListener clickListener;

    public void setList(List<TownActivity> list) {
        this.list = list;
    }

    public void setClickListener(OnTownActivityClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_card_activity, parent, false);
        return new TownActivityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TownActivity townActivity = list.get(position);
        if (holder instanceof TownActivityViewHolder) {
            TownActivityViewHolder townActivityViewHolder = (TownActivityViewHolder) holder;
            townActivityViewHolder.bindHolder(townActivity.getBackgroundUrlImage(),
                    townActivity.getName(),
                    townActivity.getDescription());
        }
    }

    @Override
    public int getItemCount() {
        int result = 0;
        if (list != null && list.size() > 0) {
            result = list.size();
        }
        return result;
    }

    public class TownActivityViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView cardImage;
        private TextView cardTitle;
        private TextView cardDescription;

        public TownActivityViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            cardImage = (ImageView) itemView.findViewById(R.id.card_image);
            cardTitle = (TextView) itemView.findViewById(R.id.card_title);
            cardDescription = (TextView) itemView.findViewById(R.id.card_text);
        }

        public void bindHolder(String imageUrl, String title, String description) {
            if (!TextUtils.isEmpty(imageUrl)) {
                Glide.with(itemView.getContext()).load(imageUrl).into(cardImage);
            }
            cardTitle.setText(title);
            cardDescription.setText(description);
        }

        @Override
        public void onClick(View view) {
            clickListener.onTownActivityClicked(list.get(getAdapterPosition()), this);
        }
    }

}
