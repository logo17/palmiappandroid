package com.loguito.palmiapp.models;

/**
 * Created by beto on 2/19/17.
 */

public class LocationTownActivityCell implements TownActivityCell {
    private String name;
    private String latitude;
    private String longitude;
    private String locationUrl;

    public LocationTownActivityCell(String name, String latitude, String longitude, String locationUrl) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.locationUrl = locationUrl;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    @Override
    public String getText() {
        return name;
    }

    @Override
    public String getImageUrl() {
        return locationUrl;
    }

    @Override
    public int getIconId() {
        return -1;
    }
}
