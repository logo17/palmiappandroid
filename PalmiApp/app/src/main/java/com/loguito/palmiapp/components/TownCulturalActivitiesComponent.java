package com.loguito.palmiapp.components;

import com.loguito.palmiapp.PalmiAppComponent;
import com.loguito.palmiapp.adapters.ActivitiesAdapter;
import com.loguito.palmiapp.common.ActivityScope;
import com.loguito.palmiapp.modules.TownActivityModule;
import com.loguito.palmiapp.presenters.TownActivityPresenter;
import com.loguito.palmiapp.ui.fragments.CulturalActivitiesFragment;
import com.loguito.palmiapp.ui.fragments.SportActivitiesFragment;

import dagger.Component;

/**
 * Created by beto on 2/19/17.
 */
@ActivityScope
@Component(
        dependencies = PalmiAppComponent.class,
        modules = TownActivityModule.class
)
public interface TownCulturalActivitiesComponent {
    void inject(CulturalActivitiesFragment fragment);

    TownActivityPresenter getPresenter();

    ActivitiesAdapter getAdapter();
}
