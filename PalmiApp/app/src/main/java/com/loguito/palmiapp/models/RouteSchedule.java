package com.loguito.palmiapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by beto on 2/21/17.
 */

public class RouteSchedule implements Parcelable {
    @SerializedName("schedule_days")
    private String scheduleDays;
    @SerializedName("schedule_options")
    private List<String> scheduleOptions;

    public RouteSchedule(String scheduleDays, List<String> scheduleOptions) {
        this.scheduleDays = scheduleDays;
        this.scheduleOptions = scheduleOptions;
    }

    public String getScheduleDays() {
        return scheduleDays;
    }

    public List<String> getScheduleOptions() {
        return scheduleOptions;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.scheduleDays);
        dest.writeStringList(this.scheduleOptions);
    }

    protected RouteSchedule(Parcel in) {
        this.scheduleDays = in.readString();
        this.scheduleOptions = in.createStringArrayList();
    }

    public static final Parcelable.Creator<RouteSchedule> CREATOR = new Parcelable.Creator<RouteSchedule>() {
        @Override
        public RouteSchedule createFromParcel(Parcel source) {
            return new RouteSchedule(source);
        }

        @Override
        public RouteSchedule[] newArray(int size) {
            return new RouteSchedule[size];
        }
    };
}
