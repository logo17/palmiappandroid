package com.loguito.palmiapp.io.callback;

import com.loguito.palmiapp.models.BusSchedule;

import java.util.List;

/**
 * Created by beto on 2/22/17.
 */

public interface GetBusScheduleListener extends ServerError {
    void onGetBusScheduleSuccess(List<BusSchedule> busSchedules);
}
