package com.loguito.palmiapp.ui.views;

import com.loguito.palmiapp.models.BusSchedule;

import java.util.List;

/**
 * Created by beto on 2/22/17.
 */

public interface BusScheduleView {
    void onGetBusScheduleSuccess(List<BusSchedule> scheduleList);
    void onGetBusScheduleFailure(String message);
    void showLoadingMessage(int messageId);
    void hideLoadingMessage();
}
