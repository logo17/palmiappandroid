package com.loguito.palmiapp.components;

import com.loguito.palmiapp.PalmiAppComponent;
import com.loguito.palmiapp.adapters.AdvicesAdapter;
import com.loguito.palmiapp.common.ActivityScope;
import com.loguito.palmiapp.modules.AdvicesModule;
import com.loguito.palmiapp.presenters.AdvicesPresenter;
import com.loguito.palmiapp.ui.fragments.AdvicesFragment;

import dagger.Component;

/**
 * Created by beto on 2/19/17.
 */
@ActivityScope
@Component(
        dependencies = PalmiAppComponent.class,
        modules = AdvicesModule.class
)
public interface AdvicesComponent {
    void inject(AdvicesFragment fragment);

    AdvicesPresenter getPresenter();

    AdvicesAdapter getAdapter();

}
