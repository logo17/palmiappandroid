# PalmiApp

## What is it?

	
	PalmiApp is an application designed for a rural but nice town. Using this application people can see updates about different topics in 
	the town (religious, cultural, etc). People can also create and share different activities (sport, cultural, educational, etc) with 
	other people around the town. The main purpose of this application is to keep the community informed with all the information posted 
	in one single application.

## Dev Tools:

*   Dagger 2 - Dependency injection
*   Retrofit - Rest calls
*   Butterknife - View injection (avoid boilerplate code)
*	Material design guidelines:
	* RecyclerView and CardView
	* Constraint Layouts
	* AppBar, Toolbars and more

## Testing Tools:

*   JUnit 4 - Unit testing
*   Espresso - Rest calls

## Architectural pattern

* Model - View - Presenter MVP