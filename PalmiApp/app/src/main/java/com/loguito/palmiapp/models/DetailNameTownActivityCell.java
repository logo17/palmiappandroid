package com.loguito.palmiapp.models;

/**
 * Created by beto on 2/19/17.
 */

public class DetailNameTownActivityCell implements TownActivityCell {

    private String name;

    public DetailNameTownActivityCell(String name) {
        this.name = name;
    }

    @Override
    public String getText() {
        return name;
    }

    @Override
    public String getImageUrl() {
        return null;
    }

    @Override
    public int getIconId() {
        return -1;
    }
}
