package com.loguito.palmiapp.presenters;

import com.loguito.palmiapp.R;
import com.loguito.palmiapp.common.BasePresenter;
import com.loguito.palmiapp.interactors.BusScheduleInteractor;
import com.loguito.palmiapp.io.callback.GetBusScheduleListener;
import com.loguito.palmiapp.models.BusSchedule;
import com.loguito.palmiapp.models.Error;
import com.loguito.palmiapp.ui.views.BusScheduleView;

import java.util.List;

/**
 * Created by beto on 2/22/17.
 */

public class BusSchedulePresenter  extends BasePresenter implements GetBusScheduleListener {

    private BusScheduleView view;
    private BusScheduleInteractor interactor;

    public BusSchedulePresenter(BusScheduleView view, BusScheduleInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    protected void onStart() {

    }

    @Override
    protected void onStop() {

    }

    public void getBusSchedule() {
        this.view.showLoadingMessage(R.string.getting_bus_schedule_message);
        this.interactor.getBusSchedule(this);
    }

    @Override
    public void onGetBusScheduleSuccess(List<BusSchedule> busSchedules) {
        this.view.hideLoadingMessage();
        this.view.onGetBusScheduleSuccess(busSchedules);
    }

    @Override
    public void onServerError(Error error) {
        this.view.hideLoadingMessage();
        this.view.onGetBusScheduleFailure(error.getEndUserMessage());
    }

}
