package com.loguito.palmiapp.ui.views;

import com.loguito.palmiapp.models.AdviceSection;

import java.util.List;

/**
 * Created by beto on 2/19/17.
 */

public interface AdvicesView {
    void onGetAdvicesSuccess(List<AdviceSection> advices);
    void onGetAdvicesFailure(String message);
    void showLoadingMessage(int messageId);
    void hideLoadingMessage();
}
