package com.loguito.palmiapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by beto on 2/18/17.
 */

public class ActivityOrganizer implements Parcelable {
    @SerializedName("name")
    private String name;
    @SerializedName("phone")
    private String phone;
    @SerializedName("email")
    private String email;
    @SerializedName("picture_url")
    private String imageUrl;

    public ActivityOrganizer(String name, String phone, String email, String imageUrl) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.phone);
        dest.writeString(this.email);
        dest.writeString(this.imageUrl);
    }

    protected ActivityOrganizer(Parcel in) {
        this.name = in.readString();
        this.phone = in.readString();
        this.email = in.readString();
        this.imageUrl = in.readString();
    }

    public static final Parcelable.Creator<ActivityOrganizer> CREATOR = new Parcelable.Creator<ActivityOrganizer>() {
        @Override
        public ActivityOrganizer createFromParcel(Parcel source) {
            return new ActivityOrganizer(source);
        }

        @Override
        public ActivityOrganizer[] newArray(int size) {
            return new ActivityOrganizer[size];
        }
    };
}
