package com.loguito.palmiapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by beto on 2/19/17.
 */

public class AdviceSection implements Parcelable {
    @SerializedName("section_name")
    private String name;
    @SerializedName("list")
    private List<AdviceDetail> details;

    public AdviceSection() {
    }

    public AdviceSection(String name, List<AdviceDetail> details) {
        this.name = name;
        this.details = details;
    }

    public String getName() {
        return name;
    }

    public List<AdviceDetail> getDetails() {
        return details;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeTypedList(this.details);
    }

    protected AdviceSection(Parcel in) {
        this.name = in.readString();
        this.details = in.createTypedArrayList(AdviceDetail.CREATOR);
    }

    public static final Parcelable.Creator<AdviceSection> CREATOR = new Parcelable.Creator<AdviceSection>() {
        @Override
        public AdviceSection createFromParcel(Parcel source) {
            return new AdviceSection(source);
        }

        @Override
        public AdviceSection[] newArray(int size) {
            return new AdviceSection[size];
        }
    };
}
