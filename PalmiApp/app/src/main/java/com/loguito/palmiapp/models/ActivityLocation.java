package com.loguito.palmiapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by beto on 2/18/17.
 */

public class ActivityLocation implements Parcelable {
    @SerializedName("place")
    private String location;
    @SerializedName("lat")
    private String latitude;
    @SerializedName("lon")
    private String longitude;

    public ActivityLocation(String location, String latitude, String longitude) {
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getLocationName() {
        return location;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLocationImageUrl() {
        return String.format("http://maps.google.com/maps/api/staticmap?center=%s,%s&zoom=15&size=400x400&sensor=false", latitude, longitude);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.location);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
    }

    protected ActivityLocation(Parcel in) {
        this.location = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
    }

    public static final Parcelable.Creator<ActivityLocation> CREATOR = new Parcelable.Creator<ActivityLocation>() {
        @Override
        public ActivityLocation createFromParcel(Parcel source) {
            return new ActivityLocation(source);
        }

        @Override
        public ActivityLocation[] newArray(int size) {
            return new ActivityLocation[size];
        }
    };
}
