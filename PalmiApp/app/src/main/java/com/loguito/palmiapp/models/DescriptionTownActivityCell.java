package com.loguito.palmiapp.models;

/**
 * Created by beto on 2/19/17.
 */

public class DescriptionTownActivityCell implements TownActivityCell {

    private String description;
    private String imageUrl;
    private int iconId;

    public DescriptionTownActivityCell(String description, String imageUrl, int iconId) {
        this.description = description;
        this.imageUrl = imageUrl;
        this.iconId = iconId;
    }

    @Override
    public String getText() {
        return description;
    }

    @Override
    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public int getIconId() {
        return iconId;
    }
}
