package com.loguito.palmiapp.components;

import android.support.v4.app.Fragment;

import com.loguito.palmiapp.PalmiAppComponent;
import com.loguito.palmiapp.adapters.ActivitiesAdapter;
import com.loguito.palmiapp.common.ActivityScope;
import com.loguito.palmiapp.modules.TownActivityModule;
import com.loguito.palmiapp.presenters.TownActivityPresenter;
import com.loguito.palmiapp.ui.fragments.SportActivitiesFragment;

import dagger.Component;

/**
 * Created by beto on 2/18/17.
 */
@ActivityScope
@Component(
        dependencies = PalmiAppComponent.class,
        modules = TownActivityModule.class
)
public interface TownSportActivitiesComponent {
    void inject(SportActivitiesFragment fragment);

    TownActivityPresenter getPresenter();

    ActivitiesAdapter getAdapter();
}
