package com.loguito.palmiapp.presenters;

import com.loguito.palmiapp.models.TownActivity;
import com.loguito.palmiapp.ui.views.TownActivityDetailView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;

import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by beto on 2/19/17.
 */
public class TownActivityDetailPresenterTest {
    @Mock
    TownActivityDetailView view;
    @Mock
    TownActivity townActivity;

    private TownActivityDetailPresenter presenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        presenter = new TownActivityDetailPresenter(view);
    }

    @Test
    public void testPrepareTownActivityDetailInformation() throws Exception {
        //when
        presenter.prepareTownActivityDetailInformation(townActivity);

        //then
        verify(view).loadTownActivityDetailInformation(anyList());
    }

}