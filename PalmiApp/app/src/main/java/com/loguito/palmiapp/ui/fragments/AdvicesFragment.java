package com.loguito.palmiapp.ui.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.loguito.palmiapp.PalmiAppComponent;
import com.loguito.palmiapp.R;
import com.loguito.palmiapp.adapters.AdvicesAdapter;
import com.loguito.palmiapp.common.BaseFragment;
import com.loguito.palmiapp.common.BasePresenter;
import com.loguito.palmiapp.components.DaggerAdvicesComponent;
import com.loguito.palmiapp.models.AdviceSection;
import com.loguito.palmiapp.modules.AdvicesModule;
import com.loguito.palmiapp.presenters.AdvicesPresenter;
import com.loguito.palmiapp.ui.views.AdvicesView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by beto on 2/19/17.
 */

public class AdvicesFragment extends BaseFragment implements AdvicesView {
    @BindView(R.id.recyclerview)
    RecyclerView recyclerViewAdvices;

    @Inject
    AdvicesPresenter presenter;
    @Inject
    AdvicesAdapter adapter;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
        presenter.getAdviceList();
    }

    private void initRecyclerView() {
        if(recyclerViewAdvices != null) {
            recyclerViewAdvices.setHasFixedSize(true);
            recyclerViewAdvices.setLayoutManager(new LinearLayoutManager(CONTEXT));
            recyclerViewAdvices.setAdapter(adapter);
        }
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_advices;
    }

    @Override
    protected void setUpComponent(PalmiAppComponent component) {
        DaggerAdvicesComponent.builder()
                .palmiAppComponent(component)
                .advicesModule(new AdvicesModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void onGetAdvicesSuccess(List<AdviceSection> advices) {
        adapter.setList(advices);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onGetAdvicesFailure(String message) {

    }

    @Override
    public void showLoadingMessage(int messageId) {
        super.showLoadingProgressBar(getString(messageId));
    }

    @Override
    public void hideLoadingMessage() {
        super.hideLoadingProgressBar();
    }
}
