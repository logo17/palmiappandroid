package com.loguito.palmiapp.ui.views;

import com.loguito.palmiapp.models.TownActivityCell;

import java.util.List;

/**
 * Created by beto on 2/19/17.
 */

public interface TownActivityDetailView {
    void loadTownActivityDetailInformation(List<TownActivityCell> list);
}
