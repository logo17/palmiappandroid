package com.loguito.palmiapp.common;

/**
 * Created by beto on 2/6/16.
 * The presenter is a bridge between views and interactors
 */
public abstract class BasePresenter {

    /**
     * It will be called in the onStart method in the view
     */
    protected abstract void onStart();

    /**
     * It will be called in the onStop method in the view
     */
    protected abstract void onStop();

}
