package com.loguito.palmiapp.io.callback;

import com.loguito.palmiapp.models.Error;

/**
 * Created by beto on 2/18/17.
 */

public interface ServerError {
    void onServerError(Error error);
}
