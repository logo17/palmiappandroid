package com.loguito.palmiapp.interactors;

import android.content.Context;

import com.loguito.palmiapp.common.MockFailurePalmiAppApiService;
import com.loguito.palmiapp.common.MockSuccessPalmiAppApiService;
import com.loguito.palmiapp.io.PalmiAppApiService;
import com.loguito.palmiapp.io.callback.GetTownActivitiesListener;
import com.loguito.palmiapp.models.Error;
import com.loguito.palmiapp.models.TownActivity;
import com.loguito.palmiapp.models.TownActivityType;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.lang.annotation.Annotation;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

/**
 * Created by beto on 2/18/17.
 */
public class TownActivityInteractorTest {
    private MockRetrofit mockRetrofit;
    private Retrofit retrofit;
    @Mock
    private PalmiAppApiService palmiAppApiService;
    @Mock
    private Context context;

    private TownActivityInteractor interactor;
    private GetTownActivitiesListener getTownActivitiesListener;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        retrofit = new Retrofit.Builder().baseUrl("http://testurl.com")
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NetworkBehavior networkBehavior = NetworkBehavior.create();
        mockRetrofit = new MockRetrofit.Builder(retrofit)
                .networkBehavior(networkBehavior)
                .build();

        getTownActivitiesListener = new GetTownActivitiesListener() {
            @Override
            public void onGetTownActivitiesSuccess(List<TownActivity> activities) {

            }

            @Override
            public void onServerError(Error error) {

            }
        };
    }

    @Test
    public void testGetSportActivitiesCalled() throws Exception {
        //given
        interactor = new TownActivityInteractor(palmiAppApiService, retrofit, context);
        TownActivityInteractor interactorSpy = Mockito.spy(interactor);

        //when
        interactorSpy.getTownActivities(getTownActivitiesListener, TownActivityType.SPORT);

        //then
        verify(palmiAppApiService).getTownActivities(TownActivityInteractor.SPORT_TYPE);
    }

    @Test
    public void testGetActivitiesDeserializeSuccess() throws Exception {
        //given
        BehaviorDelegate<PalmiAppApiService> delegate = mockRetrofit.create(PalmiAppApiService.class);
        MockSuccessPalmiAppApiService mockSuccessBaseApiService = new MockSuccessPalmiAppApiService(delegate);

        //when
        Call<List<TownActivity>> call = mockSuccessBaseApiService.getTownActivities(TownActivityInteractor.SPORT_TYPE);
        Response<List<TownActivity>> response = call.execute();

        //then
        Assert.assertNotNull(response.body());
        Assert.assertNull(response.errorBody());
        Assert.assertEquals(response.body().size(),2); // Because we add to dummy objects to the response in the @MockSuccessPalmiAppService
    }

    @Test
    public void testGetActivitiesFailure() throws Exception {
        //given
        BehaviorDelegate<PalmiAppApiService> delegate = mockRetrofit.create(PalmiAppApiService.class);
        MockFailurePalmiAppApiService mockFailureBaseApiService = new MockFailurePalmiAppApiService(delegate);

        //when
        Call<List<TownActivity>> call = mockFailureBaseApiService.getTownActivities(TownActivityInteractor.SPORT_TYPE);
        Response<List<TownActivity>> response = call.execute();

        Converter<ResponseBody, Error> errorConverter = retrofit.responseBodyConverter(Error.class, new Annotation[0]);
        Error error = errorConverter.convert(response.errorBody());

        //then
        Assert.assertFalse(response.isSuccessful());
        Assert.assertNull(response.body());
        Assert.assertNotNull(response.errorBody());
        Assert.assertEquals(404, error.getCode());
        Assert.assertEquals("Error al obtener las actividades.", error.getEndUserMessage());
        Assert.assertEquals("Unknown activity type", error.getDeveloperMessage());
    }

    @Test
    public void testGetCulturalActivitiesCalled() throws Exception {
        //given
        interactor = new TownActivityInteractor(palmiAppApiService, retrofit, context);
        TownActivityInteractor interactorSpy = Mockito.spy(interactor);

        //when
        interactorSpy.getTownActivities(getTownActivitiesListener, TownActivityType.CULTURAL);

        //then
        verify(palmiAppApiService).getTownActivities(TownActivityInteractor.CULTURAL_TYPE);
    }

}