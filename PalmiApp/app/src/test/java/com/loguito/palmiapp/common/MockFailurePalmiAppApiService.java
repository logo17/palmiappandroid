package com.loguito.palmiapp.common;

import com.loguito.palmiapp.io.PalmiAppApiService;
import com.loguito.palmiapp.models.AdviceSection;
import com.loguito.palmiapp.models.BusSchedule;
import com.loguito.palmiapp.models.TownActivity;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Path;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.Calls;

/**
 * Created by beto on 2/18/17.
 */

public class MockFailurePalmiAppApiService implements PalmiAppApiService {
    private final BehaviorDelegate<PalmiAppApiService> delegate;

    private final String getTownActivitiesJsonError = "{\n" +
            "    \"code\": 404,\n" +
            "    \"enduser_message\": \"Error al obtener las actividades.\",\n" +
            "    \"developer_message\": \"Unknown activity type\"\n" +
            "}";

    private final String getAdvicesJsonError = "{\n" +
            "    \"code\": 404,\n" +
            "    \"enduser_message\": \"Error al obtener los avisos importantes.\",\n" +
            "    \"developer_message\": \"Unknown activity type\"\n" +
            "}";

    private final String getBusScheduleJsonError = "{\n" +
            "    \"code\": 404,\n" +
            "    \"enduser_message\": \"Error al obtener el horario de buses.\",\n" +
            "    \"developer_message\": \"Unknown activity type\"\n" +
            "}";

    private final Response getTownActivitiesErrorResponse = Response.error(404, ResponseBody.create(MediaType.parse("application/json"), getTownActivitiesJsonError));
    private final Response getAdvicesErrorResponse = Response.error(404, ResponseBody.create(MediaType.parse("application/json"), getAdvicesJsonError));
    private final Response getBusScheduleErrorResponse = Response.error(404, ResponseBody.create(MediaType.parse("application/json"), getBusScheduleJsonError));

    public MockFailurePalmiAppApiService(BehaviorDelegate<PalmiAppApiService> delegate) {
        this.delegate = delegate;
    }


    @Override
    public Call<List<TownActivity>> getTownActivities(@Path("activity_type") String activityType) {
        return delegate.returning(Calls.response(getTownActivitiesErrorResponse)).getTownActivities(activityType);
    }

    @Override
    public Call<List<AdviceSection>> getTownAdvices() {
        return delegate.returning(Calls.response(getAdvicesErrorResponse)).getTownAdvices();
    }

    @Override
    public Call<List<BusSchedule>> getBusSchedule() {
        return delegate.returning(Calls.response(getBusScheduleErrorResponse)).getBusSchedule();
    }
}
