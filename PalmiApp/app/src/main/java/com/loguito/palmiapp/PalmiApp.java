package com.loguito.palmiapp;

import android.app.Application;
import android.content.Context;

/**
 * Created by beto on 2/11/17.
 */

public class PalmiApp extends Application {

    PalmiAppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        setUpComponent();
    }

    private void setUpComponent(){
        component = DaggerPalmiAppComponent
                .builder()
                .palmiAppModule(new PalmiAppModule(this))
                .build();
    }

    public PalmiAppComponent getComponent() {
        return component;
    }

    public static PalmiApp getApp(Context context) {
        return (PalmiApp) context.getApplicationContext();
    }

}