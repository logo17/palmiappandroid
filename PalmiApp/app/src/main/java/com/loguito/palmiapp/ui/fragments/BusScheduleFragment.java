package com.loguito.palmiapp.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.loguito.palmiapp.PalmiAppComponent;
import com.loguito.palmiapp.R;
import com.loguito.palmiapp.adapters.BusScheduleAdapter;
import com.loguito.palmiapp.common.BaseFragment;
import com.loguito.palmiapp.common.BasePresenter;
import com.loguito.palmiapp.common.OnBusScheduleClickListener;
import com.loguito.palmiapp.components.DaggerBusScheduleComponent;
import com.loguito.palmiapp.models.BusSchedule;
import com.loguito.palmiapp.modules.BusScheduleModule;
import com.loguito.palmiapp.presenters.BusSchedulePresenter;
import com.loguito.palmiapp.ui.activities.BusScheduleDetailActivity;
import com.loguito.palmiapp.ui.views.BusScheduleView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by beto on 2/22/17.
 */

public class BusScheduleFragment extends BaseFragment implements BusScheduleView, OnBusScheduleClickListener {
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @Inject
    BusScheduleAdapter adapter;

    @Inject
    BusSchedulePresenter presenter;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
        presenter.getBusSchedule();
    }

    private void initRecyclerView() {
        if(recyclerView != null) {
            adapter.setListener(this);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(CONTEXT));
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_bus_schedule;
    }

    @Override
    protected void setUpComponent(PalmiAppComponent component) {
        DaggerBusScheduleComponent.builder()
                .palmiAppComponent(component)
                .busScheduleModule(new BusScheduleModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void onBusScheduleClicked(BusSchedule busSchedule) {
        Intent intent = new Intent(getActivity(), BusScheduleDetailActivity.class);
        intent.putExtra(BusScheduleDetailActivity.MODEL_PARAM_NAME, busSchedule);
        getActivity().startActivity(intent);
    }

    @Override
    public void onGetBusScheduleSuccess(List<BusSchedule> scheduleList) {
        adapter.setSchedules(scheduleList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onGetBusScheduleFailure(String message) {

    }

    @Override
    public void showLoadingMessage(int messageId) {
        super.showLoadingProgressBar(getString(messageId));
    }

    @Override
    public void hideLoadingMessage() {
        super.hideLoadingProgressBar();
    }
}
