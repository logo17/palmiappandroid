package com.loguito.palmiapp.common;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.loguito.palmiapp.PalmiApp;
import com.loguito.palmiapp.PalmiAppComponent;
import com.loguito.palmiapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by beto on 2/6/16.
 */
public abstract class BaseFragment extends Fragment {

    protected Context CONTEXT;

    @Nullable
    @BindView(R.id.progress_container)
    View progressContainer;
    @Nullable
    @BindView(R.id.progress_text)
    TextView progressText;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        CONTEXT = context;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
        injectDependencies();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getFragmentLayout(), container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(getPresenter()!=null){
            getPresenter().onStart();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(getPresenter()!=null){
            getPresenter().onStop();
        }
    }

    protected void showLoadingProgressBar(String message) {
        if(progressContainer != null && progressText != null) {
            progressText.setText(message);
            progressContainer.setVisibility(View.VISIBLE);
        }
    }

    protected void hideLoadingProgressBar() {
        if(progressContainer != null && progressText != null) {
            progressText.setText(null);
            progressContainer.setVisibility(View.GONE);
        }
    }

    /**
     * @return The presenter attached to the fragment. This must extends from BasePresenter}
     * */
    protected abstract BasePresenter getPresenter();

    /*
    It will be decide which layout inflate
     */
    protected abstract int getFragmentLayout();


    /**
     * It will be prepare butterknife for inflating all views
     * @param rootView
     */
    private void bindViews(View rootView) {
        ButterKnife.bind(this, rootView);
    }

    /**
     * It will be inject all dagger dependencies for this fragment
     */
    private void injectDependencies(){
        setUpComponent(PalmiApp.getApp(CONTEXT).getComponent());
    }

    protected abstract void setUpComponent(PalmiAppComponent component);
}
