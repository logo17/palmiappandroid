package com.loguito.palmiapp.ui.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.widget.TextView;

import com.loguito.palmiapp.PalmiAppComponent;
import com.loguito.palmiapp.R;
import com.loguito.palmiapp.common.BaseActivity;
import com.loguito.palmiapp.ui.fragments.ActivitiesPagerFragment;
import com.loguito.palmiapp.ui.fragments.AdvicesFragment;
import com.loguito.palmiapp.ui.fragments.BusScheduleFragment;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by beto on 1/31/16.
 */
public class MainActivity extends BaseActivity{
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    CircleImageView userPhoto;
    TextView userName;
    TextView userEmail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpToolbar("",true);
        setupDrawerContent();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected Fragment createFragment() {
        return new ActivitiesPagerFragment();
    }

    @Override
    protected void setUpToolbar(String title, boolean hasActionButton) {
        super.setUpToolbar(title, hasActionButton);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
    }


    private void setupDrawerContent() {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // Mark item as pressed
                        menuItem.setChecked(true);
                        // Create new fragment an inflate it
                        String title = menuItem.getTitle().toString();
                        setTitle(title);
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.fragment_container, selectFragment(menuItem.getItemId()))
                                .commit();
                        drawerLayout.closeDrawers(); // Cerrar drawer
                        return true;
                    }
                }
        );
    }

    /**
     *
     * @param id identifier of a fragment that should be returned
     * @return
     */
    private Fragment selectFragment(int id) {
        Fragment fragment;
        switch (id) {
            case R.id.nav_cultural_activity:
                fragment = new ActivitiesPagerFragment();
                break;
            case R.id.nav_important_advices:
                fragment = new AdvicesFragment();
                break;
            case R.id.nav_bus_schedule:
                fragment = new BusScheduleFragment();
                break;
            default:
                fragment = new ActivitiesPagerFragment();
                break;
        }
        return fragment;
    }

}
