package com.loguito.palmiapp.io;

import com.loguito.palmiapp.models.AdviceSection;
import com.loguito.palmiapp.models.BusSchedule;
import com.loguito.palmiapp.models.TownActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by beto on 2/11/17.
 */

public interface PalmiAppApiService {
    @GET("/activities/{activity_type}")
    Call<List<TownActivity>> getTownActivities(@Path("activity_type") String activityType);

    @GET("/advices")
    Call<List<AdviceSection>> getTownAdvices();

    @GET("/schedules/bus")
    Call<List<BusSchedule>> getBusSchedule();
}
